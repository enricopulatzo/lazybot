(ns lazybot.plugins.stats
  (:require [clojure.string :as string])
  (:use [lazybot registry]
        [somnium.congomongo :only [fetch fetch-one fetch-count insert! destroy! update!]]))

(comment
  (count (fetch :feelings))
  (count (fetch :wishes))
  (count (fetch :whatis))
  (count (fetch :mail))
  (count (fetch :karma))
  (count (fetch :help))
  (count (fetch :seen))
  (fetch-count :wishes)
  )

(defplugin 
  (:cmd
    "Show some statistics about all the crap we've been feeding this bot."
    #{"stats"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m 
                    (str "I know about "
                          (fetch-count :whatis) " facts, "
                          (fetch-count :feelings) " feelings, "
                          (fetch-count :wishes) " wishes, "
                          (fetch-count :karma) " karma records, "
                          (fetch-count :seen) " @seen records, "
                          (fetch-count :fortune) " fortune cookies, "
                          " and probably some other stuff."
                          )
      )
    )
  ))
