(ns lazybot.plugins.hackernews 
  (:use [lazybot registry]
        [clojure.data.json :only [read-json]]) 
  (:require [clj-http.client :as client])
  (:import org.apache.commons.lang.StringEscapeUtils)
  )

; both APIs return similar JSON
; but ihackernews seems faster but unreliable (500s a lot)
  ; and returns the score as :points
; and hndroidapi seems to require some html entity decoding
  ; and returns the score as :score

(def HN-URL "http://api.ihackernews.com/page")
;(def HN-URL "https://hndroidapi.appspot.com/news/format/json/page/?appid=&callback=")

(defn random-hn-item
  "pull a random story from the front page"
  [] 
  (-> (client/get HN-URL)
    :body 
    read-json
    :items
    rand-nth
    #_(update-in [:title] (memfn StringEscapeUtils/unescapeHtml))
    ))

(comment
  (prn x)
  (def x (try
           (random-hn-item)
           (catch Throwable e
             e))) 
  (org.apache.commons.lang.StringEscapeUtils/unescapeHtml "foo &trade; bar")
  )

(defplugin
  (:cmd
    "show a random story from the front page of Hacker News"
    #{"hackernews" "hn"}
    (fn [com-m]
      (try
        (let [{:keys [title points score url]} (random-hn-item) ]
          (send-message com-m (str title " (score: " (or points score) ") " url)))
        (catch Exception e
          (send-message com-m (str "sorry, got an exception trying to reach HN: " e)))))))

