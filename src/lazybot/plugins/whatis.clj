(ns lazybot.plugins.whatis
  (:use [lazybot registry]
        [somnium.congomongo :only [fetch fetch-one insert! destroy!]]))

(defn tell-about [what com-m]
  (send-message com-m
                (str what
                     (if-let [result (fetch-one :whatis :where {:subject what})]
                       (str " is " (:is result))
                       " does not exist in my database."))))

(def learn-responses
  ["My memory is more powerful than M-x butterfly. I won't forget it."
   "Got it, boss."
   "Sure, let me just put that in my MongoDB for you."
   "If you insist."
   "Okay, learned."
   "Learned. Someone is regularly backing up my DB, right?"
   "Got it. But you realize probably nobody will look that up unless they're doing @randomfact out of boredom."
   "You don't say?"
   "For real?  Okay."
   "I'll never forget that. (Unless you @forget it.)"
   "The Moving Finger writes; and, having writ, Moves on: nor all thy Piety nor Wit Shall lure it back to cancel half a Line, Nor all thy Tears wash out a Word of it.  But @forget works."
   ])


(defplugin 
  (:cmd
   "Teaches the bot a new thing. It takes a name and whatever you want to assign the name
   to. For example: $learn me a human being."
   #{"learn"} 
   (fn [{:keys [args] :as com-m}]
     (let [[subject & is] args
           is-s (apply str (interpose " " is))]
       (do
         (destroy! :whatis {:subject subject})
         (insert! :whatis {:subject subject :is is-s})
         (send-message com-m (rand-nth learn-responses))))))
   

  #_(:cmd
    "Teaches the bot a new fact. Facts must include the word 'is' or 'are'.
    Example: `@learn my IRC bot is being a dick` would use 'my IRC bot' as a key and would store the phrase 'is being a dick'
    for later lookup, use `@whatis my IRC bot`."
    #{"learn2"}
    (fun [{:keys [args] :as com-m}]
        (let [input        (apply str (interpose " " is))
              [key phrase] (map string/trim 
                                (string/split #"\b(is|are)\b" 2))] 
          (do
            (if (and key phrase)
              (do
                (destroy! :whatis {:subject subject})
                (insert! :whatis {:subject key, :is phrase})
                (send-message com-m (rand-nth learn-responses)))
              (do
                (send-message com-m "I can only learn phrases including 'is' of the form: @learn my example is pretty simple")
                ))
            ))))

   (:cmd 
    "Pass it a key, and it will tell you what is at the key in the database."
    #{"whatis" "what-is"}
    (fn [{[what] :args :as com-m}]
      (tell-about what com-m)))

   (:cmd 
    "Pass it a key, and it will tell the recipient what is at the key in the database via PM
Example - $tell G0SUB about clojure"
    #{"tell"}
    (fn [{[who _ what] :args :as com-m}]
      (when what
        (tell-about what (assoc com-m :channel who)))))
   
   (:cmd 
    "Forgets the value of a key."
    #{"forget"} 
    (fn [{[what] :args :as com-m}]
      (do (destroy! :whatis {:subject what})
          (send-message com-m (str "If " what " was there before, it isn't anymore. R.I.P.")))))

   (:cmd 
    "Gets a random value from the database."
    #{"rwhatis" "randomfact" "random-fact"} 
    (fn [com-m]
      (let [what (-> :whatis fetch rand-nth :subject)]
        (tell-about what com-m))))
   (:indexes [[:subject]]))

