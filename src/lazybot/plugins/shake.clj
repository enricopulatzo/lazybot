(ns lazybot.plugins.shake
  (:use lazybot.registry
        [lazybot.utilities :only [prefix]]))

;; contributed by J. Andrew Thompson 2013-08-22
;
(def verbs
  [
     "bawdy"
     "beslubbering"
     "bootless"
     "churlish"
     "cockered"
     "clouted"
     "craven"
     "currish"
     "dankish"
     "dissembling"
     "droning"
     "errant"
     "fawning"
     "fobbing"
     "froward"
     "frothy"
     "gleeking"
     "goatish"
     "gorbellied"
     "impertinent"
     "infectious"
     "jarring"
     "loggerheaded"
     "lumpish"
     "mammering"
     "mangled"
     "mewling"
     "paunchy"
     "pribbling"
     "puking"
     "puny"
     "qualling"
     "rank"
     "reeky"
     "roguish"
     "ruttish"
     "saucy"
     "spleeny"
     "spongy"
     "surly"
     "tottering"
     "unmuzzled"
     "vain"
     "venomed"
     "villainous"
     "warped"
     "wayward"
     "weedy"
     "yeasty"
   ]
  )

(def adjectives 
  [
    "base-court"
    "bat-fowling"
    "beef-witted"
    "beetle-headed"
    "boil-brained"
    "clapper-clawed"
    "clay-brained"
    "common-kissing"
    "crook-pated"
    "dismal-dreaming"
    "dizzy-eyed"
    "doghearted"
    "dread-bolted"
    "earth-vexing"
    "elf-skinned"
    "fat-kidneyed"
    "fen-sucked"
    "flap-mouthed"
    "fly-bitten"
    "folly-fallen"
    "fool-born"
    "full-gorged"
    "guts-griping"
    "half-faced"
    "hasty-witted"
    "hedge-born"
    "hell-hated"
    "idle-headed"
    "ill-breeding"
    "ill-nurtured"
    "knotty-pated"
    "milk-livered"
    "motley-minded"
    "onion-eyed"
    "plume-plucked"
    "pottle-deep"
    "pox-marked"
    "reeling-ripe"
    "rough-hewn"
    "rude-growing"
    "rump-fed"
    "shard-borne"
    "sheep-biting"
    "spur-galled"
    "swag-bellied"
    "tardy-gaited"
    "tickle-brained"
    "toad-spotted"
    "unchin-snouted"
   ]
)

(def nouns
  [
    "apple-john"
    "baggage"
    "barnacle"
    "bladder"
    "boar-pig"
    "bugbear"
    "bum-bailey"
    "canker-blossom"
    "clack-dish"
    "clotpole"
    "coxcomb"
    "codpiece"
    "death-token"
    "dewberry"
    "flap-dragon"
    "flax-wench"
    "flirt-gill"
    "foot-licker"
    "fustilarian"
    "giglet"
    "gudgeon"
    "haggard"
    "harpy"
    "hedge-pig"
    "horn-beast"
    "hugger-mugger"
    "joithead"
    "lewdster"
    "lout"
    "maggot-pie"
    "malt-worm"
    "mammet"
    "measle"
    "minnow"
    "miscreant"
    "moldwarp"
    "mumble-news"
    "nut-hook"
    "pigeon-egg"
    "pignut"
    "puttock"
    "pumpion"
    "ratsbane"
    "scut"
    "skainsmate"
    "strumpet"
    "varlot"
    "vassal"
    "whey-face"
    "wagtail" 
   ]
  )

(def addressee [
             "being a"
             "a"
             "most certainly a"
             ])

(defn get-insult []
  (clojure.string/join " "  
                       (for  [type  [addressee verbs adjectives nouns]]  
                         (rand-nth type)))

  )

(defn insult-x [x]
  (str x ": "
       (get-insult))

  )
(defplugin
  (:cmd
   "Build a Shakespearean Insult.  Ex. '@insult Your Momma'"
   #{"shake" "insult"}
   (fn [{:keys [com bot nick channel args] :as com-m}]
     (str @com)
     (send-message com-m 
                   (if (< 0 (count args))
                     (insult-x (clojure.string/join " " args))
                     (insult-x nick) 
                     ))))

  )


