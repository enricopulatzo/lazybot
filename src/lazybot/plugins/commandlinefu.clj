(ns lazybot.plugins.commandlinefu 
  (:use [lazybot registry]
        [clojure.data.json :only [read-json]]) 
  (:require [clj-http.client :as client])
  (:import org.apache.commons.lang.StringEscapeUtils)
  )

; let's also make a plugin for the commandlinefu api
; http://www.commandlinefu.com/site/api
; http://www.commandlinefu.com/commands/random/json

(def CLFU-URL "http://www.commandlinefu.com/commands/random/json")

; keys :command :summary :votes :id :url
(defn random-clfu []
  (-> (client/get CLFU-URL)
    :body
    read-json
    first
    ))

(defn display-clfu
  "turn a command line fu item into a sequence of messages"
  [clfu]
  [(str "summary: " (:summary clfu))
   (str "command: " (:command clfu))
   (str "link: " (:url clfu))
   ])

(comment
  (display-clfu (random-clfu))

  ; yields for example
  ["summary: Update zone file Serial numbers" 
   "command: sed -i 's/20[0-1][0-9]\\{7\\}/'`date +%Y%m%d%I`'/g' *.db" 
   "link: http://www.commandlinefu.com/commands/view/5136/update-zone-file-serial-numbers"]

  )


(defplugin
  (:cmd
    "display a random command from commandlinefu"
    #{"random-command" "clfu" "random-clfu" "randomcommand" "unix-tip" "unixtip"}
    (fn [com-m]
      (try
        (doseq [message (display-clfu (random-clfu))]
          (send-message com-m message))
        (catch Exception e
          (send-message com-m (str "sorry, got an exception trying to reach clfu: " e)))))))


