(ns lazybot.plugins.uptime
  (:use [lazybot registry])
  (:use [clojure.java.shell :only [sh]])
  (:require [clojure.string :as string]))

(defn prefix-nick 
  [nick & stuff]
  (apply str nick ": " stuff))

; this parsing is pretty ad-hoc and brittle
(defn show-uptime []
  (let [[days mins] (-> (sh "uptime")
                      :out
                      (string/split #",")
                      (->> (take 2))
                      ) 
        days       (-> days
                     (string/split #" ") 
                     (->> 
                       (drop 2)
                       (string/join " ")))
        mins      (string/trim mins) ]
    (str "My host machine has been " days ", " mins)))

; (show-uptime)

(defplugin
  (:cmd
    "see how long the bot's host machine has been up"
    #{"uptime"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m (prefix-nick nick (show-uptime))))))

